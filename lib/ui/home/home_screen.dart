
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sample_flutter/contants.dart';
import 'package:sample_flutter/ui/home/component/body.dart';

class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppbar(),
      body: ProductBody(),
    );
  }

  AppBar _buildAppbar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      title: Text(
        "Online Shop",
        style: TextStyle(
          color: Colors.black87
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: SvgPicture.asset(
            "assets/icons/search.svg",
            color: consTextColor,
          ),
          onPressed: () {},
        ),
        IconButton(
          icon: SvgPicture.asset(
            "assets/icons/cart.svg",
            color: consTextColor,
          ),
          onPressed: () {},
        ),
        SizedBox(width: consDefaultPadding / 2)
      ],
    );
  }

}
