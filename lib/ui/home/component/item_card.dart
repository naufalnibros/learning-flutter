import 'package:flutter/material.dart';
import 'package:sample_flutter/contants.dart';
import 'package:sample_flutter/models/Product.dart';

class ItemCard extends StatelessWidget {
  final Product product;
  final Function press;

  const ItemCard({Key key, this.product, this.press}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(consDefaultPadding),
              decoration: BoxDecoration(
                color: product.color,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Hero(
                tag: "${product.id}",
                child: Image.asset(product.image),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: consDefaultPadding / 4),
            child: Text(
              product.title,
              style: TextStyle(color: consTextLightColor),
            ),
          ),
          Text(
            "Rp.${product.price},00",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 8)
        ],
      ),
    );
  }
}
