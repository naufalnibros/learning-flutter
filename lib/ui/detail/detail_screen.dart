import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sample_flutter/contants.dart';
import 'package:sample_flutter/models/Product.dart';
import 'package:sample_flutter/ui/detail/component/body.dart';

class DetailScreen extends StatelessWidget {

  final Product product;

  const DetailScreen({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: product.color,
      appBar: _buildAppbar(context),
      body: DetailBody(product: product),
    );
  }

  Widget _buildAppbar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: product.color,
      leading: IconButton(
        icon: SvgPicture.asset(
          'assets/icons/back.svg',
          color: Colors.white,
        ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[
        IconButton(
          icon: SvgPicture.asset("assets/icons/search.svg"),
          onPressed: () {},
        ),
        IconButton(
          icon: SvgPicture.asset("assets/icons/cart.svg"),
          onPressed: () {},
        ),
        SizedBox(width: consDefaultPadding / 2)
      ],
    );
  }

}
