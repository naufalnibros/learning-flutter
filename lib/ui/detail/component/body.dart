
import 'package:flutter/material.dart';
import 'package:sample_flutter/contants.dart';
import 'package:sample_flutter/models/Product.dart';
import 'package:sample_flutter/ui/detail/component/description.dart';
import 'package:sample_flutter/ui/detail/component/favorite_action.dart';
import 'package:sample_flutter/ui/detail/component/product_title_image.dart';

import 'add_to_cart.dart';
import 'color_and_size.dart';

class DetailBody extends StatelessWidget {
  final Product product;

  const DetailBody({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: size.height * 1,
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.31),
                  padding: EdgeInsets.only(
                    top: size.height * 0.12,
                    left: consDefaultPadding,
                    right: consDefaultPadding,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24),
                    ),
                  ),
                    child: Column(
                      children: <Widget>[
                        ColorAndSize(product: product),
                        Description(product: product),
                        SizedBox(height: consDefaultPadding / 2),
                        ProductAction(),
                        SizedBox(height: consDefaultPadding / 2),
                        AddToCart(product: product)
                      ],
                    ),
                ),
                ProductTitleImage(product: product)
              ],
            ),
          )
        ],
      ),
    );
  }
}
