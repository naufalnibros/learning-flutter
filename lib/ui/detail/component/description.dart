import 'package:flutter/material.dart';
import 'package:sample_flutter/contants.dart';
import 'package:sample_flutter/models/Product.dart';

class Description extends StatelessWidget {
  final Product product;

  const Description({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: consDefaultPadding),
        child: RichText(
          text: TextSpan(style: TextStyle(color: consTextColor), children: [
            TextSpan(text: "Description\n", style: TextStyle(fontWeight: FontWeight.bold)),
            TextSpan(text: product.description, style: TextStyle(height: 1.6))
          ]),
        ));
  }
}
