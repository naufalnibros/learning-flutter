import 'package:flutter/material.dart';
import 'package:sample_flutter/contants.dart';

class CartCounter extends StatefulWidget {
  @override
  _CartCounterState createState() => _CartCounterState();
}

class _CartCounterState extends State<CartCounter> {

  int _incrementFavorite = 1;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _buildOutlineButton(
          icon: Icons.remove,
          press: () {
            if (_incrementFavorite > 1) {
              setState(() {
                _incrementFavorite--;
              });
            }
          }
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: consDefaultPadding / 2),
          child: Text(
            _incrementFavorite.toString().padLeft(2, "0"),
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        _buildOutlineButton(
          icon: Icons.add,
          press: () {
            setState(() {
              _incrementFavorite++;
            });
          }
        )
      ],
    );
  }

  Widget _buildOutlineButton({IconData icon, Function press}) {
    return SizedBox(
      width: 40,
      height: 32,
      child: OutlineButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
        onPressed: press,
        child: Icon(icon),
      ),
    );
  }

}
