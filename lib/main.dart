import 'package:flutter/material.dart';
import 'package:sample_flutter/contants.dart';
import 'package:sample_flutter/ui/home/home_screen.dart';

void main() {
  runApp(CoreApplication());
}

class CoreApplication extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Example",
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
          bodyColor: consTextColor
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity
      ),
      home: HomeScreen(),
    );
  }

}