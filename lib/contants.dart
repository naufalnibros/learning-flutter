import 'package:flutter/material.dart';

const consTextColor = Color(0xFF535353);
const consTextLightColor = Color(0xFFACACAC);

const consDefaultPadding = 20.0;
